resource "kubernetes_service_account" "account" {
  metadata {
    name      = var.username
    namespace = var.namespace
  }
  automount_service_account_token = true
}

resource "kubernetes_secret" "secret" {
  metadata {
    name      = "${var.username}-token"
    namespace = var.namespace
    annotations = {
      "kubernetes.io/service-account.name" = kubernetes_service_account.account.metadata[0].name
    }
  }
  type = "kubernetes.io/service-account-token"
}

data "kubernetes_secret" "secret" {
  metadata {
    name      = kubernetes_secret.secret.metadata[0].name
    namespace = var.namespace
  }
}

resource "kubernetes_role_binding" "binding" {
  metadata {
    name = "${var.username}-binding"
    labels = {
      project = local.project_name
    }
    namespace = var.namespace
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = var.role_name
  }
  subject {
    kind      = "ServiceAccount"
    name      = kubernetes_service_account.account.metadata[0].name
    namespace = var.namespace
  }
}
locals {
  context = "${var.username}@${var.cluster_name}-${var.namespace}"
  cluster = "${var.cluster_name}-${var.namespace}"
  user    = "${var.cluster_name}-${var.username}-${var.namespace}"
}
resource "null_resource" "kubeconfig" {
  provisioner "local-exec" {
    command     = <<EOT
    cat << EOF > ${var.filename}
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: ${var.cluster_ca_certificate}
    server: ${var.cluster_host}
  name: ${local.cluster}
contexts:
- context:
    cluster: ${local.cluster}
    namespace: ${var.namespace}
    user: ${local.user}
  name: ${local.context}
current-context: ${local.context}
kind: Config
preferences: {}
users:
- name: ${local.user}
  user:
    token: ${lookup(data.kubernetes_secret.secret.data, "token")}
  EOT
    interpreter = ["bash", "-c"]
  }
}
