variable "namespace" {
  type = string

  description = "Existing Kubernetes namespace to create the kubeconfig for."
}

variable "username" {
  type = string

  description = "Usename for the Service Account to be created. Can be anything, but usually the name of the person that will be using the kubeconfig is used. Needs to be unique within each namespace."
}

variable "project_name" {
  type = string

  description = "Project name to label the Role Binding. Can be anything."
}
locals {
  project_name = replace(var.project_name, " ", "")
}

variable "role_name" {
  type = string

  description = "Existing Role that should be bound to the created Service Account."
}

variable "filename" {
  type    = string
  default = "kubeconfig.yml"

  description = "Name of the file used for output. Make is unique if you're generating several kubeconfigs i.e. for several namespaces/clusters at the same time."
}

variable "cluster_name" {
  type    = string
  default = "igloo"

  description = "Name of the cluster in the kubeconfig file. Can be anything, will only impact the names of the cluster, context and user that will be seen in kubectl commands."
}

variable "cluster_host" {
  type = string

  description = "The URL of the Kubernetes API server."
}

variable "cluster_ca_certificate" {
  type = string

  description = "The CA certificate of the Kubernetes API server."
}
