# Generate scoped kubeconfig

This terraform module allows generating kubeconfig files with a scope limited to a specific namespace.
It generates the files at the path from where the `terraform apply` command is called.
Because of this, applying any configuration from this module should be done manually and not through an automated CI pipeline.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_kubernetes"></a> [kubernetes](#provider\_kubernetes) | n/a |
| <a name="provider_null"></a> [null](#provider\_null) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [kubernetes_role_binding.binding](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/role_binding) | resource |
| [kubernetes_service_account.account](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/service_account) | resource |
| [null_resource.kubeconfig](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [kubernetes_secret.secret](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/data-sources/secret) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_cluster-name"></a> [cluster-name](#input\_cluster-name) | Name of the cluster in the kubeconfig file. Can be anything, will only impact the names of the cluster, context and user that will be seen in kubectl commands. | `string` | `"igloo"` | no |
| <a name="input_cluster_ca_certificate"></a> [cluster\_ca\_certificate](#input\_cluster\_ca\_certificate) | The CA certificate of the Kubernetes API server. | `string` | n/a | yes |
| <a name="input_cluster_host"></a> [cluster\_host](#input\_cluster\_host) | The URL of the Kubernetes API server. | `string` | n/a | yes |
| <a name="input_filename"></a> [filename](#input\_filename) | Name of the file used for output. Make is unique if you're generating several kubeconfigs i.e. for several namespaces/clusters at the same time. | `string` | `"kubeconfig.yml"` | no |
| <a name="input_namespace"></a> [namespace](#input\_namespace) | Existing Kubernetes namespace to create the kubeconfig for. | `string` | n/a | yes |
| <a name="input_project_name"></a> [project\_name](#input\_project\_name) | Project name to label the Role Binding. Can be anything. | `string` | n/a | yes |
| <a name="input_role_name"></a> [role\_name](#input\_role\_name) | Existing Role that should be bound to the created Service Account. | `string` | n/a | yes |
| <a name="input_username"></a> [username](#input\_username) | Usename for the Service Account to be created. Can be anything, but usually the name of the person that will be using the kubeconfig is used. Needs to be unique within each namespace. | `string` | n/a | yes |

## Outputs

No outputs.
<!-- END_TF_DOCS -->